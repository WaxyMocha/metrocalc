//
// Created by waxy on 24.11.19.
//

#ifndef METROCALC_UTILS_H
#define METROCALC_UTILS_H

#include <string>

using namespace std;

string convert_to_human_readable(double value, const string &unit);

string remove_zeros(string str);

#endif //METROCALC_UTILS_H
