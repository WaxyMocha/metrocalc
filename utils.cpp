#include <cmath>
#include "utils.h"


string convert_to_human_readable(double value, const string &unit) {
    string down[] = {
            "m",
            "µ",
            "n",
            "p"
    };
    string up[] = {
            "",
            "k",
            "M"
    };
    if (value >= 1) {
        int power = int(log10(value)) / 3;
        return remove_zeros(to_string(value / (pow(10, power * 3)))) + up[power] + unit;
    } else {
        int power = int(log10(value) + 1) / 3;
        return remove_zeros(to_string(value / (pow(10, (power - 1) * 3)))) + down[abs(power)] + unit;
    }
}

string remove_zeros(string str) {
    int i = str.length() - 1;
    for (; str[i] == '0'; i--);

    str = str.substr(0, i);

    if (str.empty()) {
        str = "0";
    }

    return str;
}